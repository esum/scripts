#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

import auth
import sys
import time

delattr(sys, 'argv')

#if len(sys.argv) < 2 and False:
#    print "Give me a mac !"
#    sys.exit(1)

# Machine à s'authentifier (cerveaulent)
#p=(('Calling-Station-Id', sys.argv[1]),)

auth.instantiate(())

# Test avec l'interface wifi d'apprentis
p=(
    ('Calling-Station-Id', '02:69:75:42:24:03'),
    ('User-Name', 'apprentis-wifi'),
)

print repr(auth.authorize_wifi(p))
print "wait for 3s, tu peux aller crasher le serveur pg ou ldap"
sys.stdout.flush()

time.sleep(3)

print repr(auth.post_auth_wifi(p))
