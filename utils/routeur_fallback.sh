#!/bin/bash

# Script à exécuter en tant que root pour mettre en place rapidement le
# routeur de fallback lors d'une maintenance ou d'une coupure du routeur
# principal

if "$(whoami)" != "root"; then
    echo "Ce script doit être exécuté en tant que root !"
    exit 1
fi

# Paramètres du routeur principal que l'on doit usurper

GATEWAY_ADH="138.231.136.4/21"
GATEWAY_ADM="10.231.136.4/24"
GATEWAY_WIFI="138.231.148.4/21"
GATEWAY_BORNES="10.231.148.4/24"
GATEWAY_APP="10.2.9.4/24"
GATEWAY_FEDEREZ="10.53.0.4/16"

GATEWAY_6="fe80::1/64"

GATEWAY_ZRT="138.231.132.1"
GATEWAY_CRANS="138.231.136.4"

IP_RADIUS_BORNES="10.231.148.11/24"
IP_RADIUS_SWITCHS="10.231.136.11/24"

IP_DHCP_ADH="138.231.136.34/21"
IP_DHCP_ADM="10.231.136.34/24"
IP_DHCP_WIFI="138.231.148.34/21"
IP_DHCP_BORNES="10.231.148.34/24"
IP_DHCP_APP="10.2.9.34/24"
IP_DHCP_FEDEREZ="10.53.0.34/16"

IP_DNS_ADH="138.231.136.152/21"
IP_DNS_ADM="10.231.136.152/24"

take_radius()
{
    echo -n "Spoof des IPs du radius... "
    # On vole les IPs du radius sur les différentes interfaces
    ip a add $IP_RADIUS_BORNES dev eth0.3
    echo -n "[adm($IP_RADIUS_BORNES)]"
    ip a add $IP_RADIUS_SWITCHS dev eth0.2
    echo -n "[adm($IP_RADIUS_SWITCHS)]"
    systemctl restart freeradius
    echo "[OK]"
}

take_dns()
{
    echo -n "Spoof des IPs du dns récursif... "
    # On vole les IPs du dns sur les différentes interfaces
    ip a add $IP_DNS_ADH dev eth0
    echo -n "[adm($IP_DNS_ADH)]"
    ip a add $IP_DNS_ADM dev eth0.2
    echo -n "[adm($IP_DNS_ADM)]"
    systemctl restart bind9
    echo "[OK]"
}


take_dhcp()
{
    echo -n "Spoof des IPs du dhcp... "
    # On vole les IPs du dhcp sur les différentes interfaces
    ip a add $IP_DHCP_ADH dev eth0
    echo -n "[adh($IP_DHCP_ADH)]"
    ip a add $IP_DHCP_ADM dev eth0.2
    echo -n "[adm($IP_DHCP_ADM)]"
    ip a add $IP_DHCP_WIFI dev eth0.3
    echo -n "[adm($IP_DHCP_WIFI)]"
    ip a add $IP_DHCP_BORNES dev eth0.3
    echo -n "[adm($IP_DHCP_BORNES)]"
    ip a add $IP_DHCP_APP dev eth0.21
    echo -n "[adm($IP_DHCP_APP)]"
    ip a add $IP_DHCP_FEDEREZ dev eth0.22
    echo -n "[adm($IP_DHCP_FEDEREZ)]"
    systemctl restart isc-dhcp-server
    echo "[OK]"
}


applying_for_gateway()
{
    echo -n "Phase 1: Spoof des IPs du routeur... "
    # On vole les IPs du routeur sur les différentes interfaces
    ip a add $GATEWAY_ADH dev eth0
#    ip a add $GATEWAY_6 dev eth0
    echo -n "[adhérents($GATEWAY_ADH + $GATEWAY_6)]"
    ip a add $GATEWAY_ADM dev eth0.2
    echo -n "[adm($GATEWAY_ADM)]"
    ip a add $GATEWAY_WIFI dev eth0.3
#    ip a add $GATEWAY_6 dev eth0.3
    echo -n "[wifi($GATEWAY_WIFI + $GATEWAY_6)]"
    ip a add $GATEWAY_BORNES dev eth0.3
    echo -n "[bornes($GATEWAY_BORNES)]"
    ip a add $GATEWAY_APP dev eth0.21
    echo -n "[appartements($GATEWAY_APP)]"
    ip a add $GATEWAY_FEDEREZ dev eth0.22
    echo -n "[federez($GATEWAY_FEDEREZ)]"

    # Faudra quand même envoyer les paquets vers l'Internet à un moment
    echo -n "Changement de route IPv4 par défaut ($GATEWAY_ZRT) ... "
    ip r replace default via $GATEWAY_ZRT dev eth1 src 138.231.136.4
    echo "[OK]"
}

setup_IPv4()
{
    # On se fait passer pour le routeur auprès de l'ENS, puis c'est censé
    # marcher tout seul
    echo -n "Phase 2: Échange de routes avec l'ENS ... "
    systemctl start quagga
    echo "[quagga]"
}

setup_IPv6()
{
    # On fait pareil avec SixXS, moyennant quelques RAs, et c'est censé marcher
    # tout seul
    echo "Phase 3: Mise en place de l'IPv6 ... "
    echo "Démarrage de AICCU ..."
    echo "ATTENTION /!\\ Le tunnel SixXS sur le routeur principal doit être coupé !"
    echo "Est-ce le cas ? [o/N]"
    read AICCU_SAFE
    case "$AICCU_SAFE" in
        o|O|y|Y|oui|yes)
	    systemctl start aiccu
	    echo "[aiccu]"
            ;;
        *)
            echo "AICCU n'a PAS été démarré."
	    ;;
    esac
    systemctl start radvd
    echo "[radvd]"
}

start_firewall()
{
    # On démarre le firewall à la fin, ça peut être utile.
    # Même sans le comptage d'upload.
    echo -n "Phase 4: Démarrage des firewalls ... "
    systemctl restart firewall
    echo -n "[Firewall 4]"
    #systemctl restart firewall6
    #echo "[Firewall 6]"
}

being_fired()
{
    # Toutes les IPs spoofées doivent être oubliées
    echo -n "Phase 3: Retrait des IPs du routeur ... "
    ip a del $GATEWAY_ADH dev eth0
    ip a del $GATEWAY_ADM dev eth0.2
    ip a del $GATEWAY_WIFI dev eth0.3
    ip a del $GATEWAY_BORNES dev eth0.3
    ip a del $GATEWAY_APP dev eth0.21
    ip a del $GATEWAY_FEDEREZ dev eth0.22
    ip a del $IP_RADIUS_SWITCHS dev eth0.2
    ip a del $IP_RADIUS_BORNES dev eth0.3
    ip a del $IP_DHCP_ADH dev eth0
    ip a del $IP_DHCP_ADM dev eth0.2
    ip a del $IP_DHCP_WIFI dev eth0.3
    ip a del $IP_DHCP_BORNES dev eth0.3
    ip a del $IP_DHCP_APP dev eth0.21
    ip a del $IP_DHCP_FEDEREZ dev eth0.22
    ip a del $IP_DNS_ADH dev eth0
    ip a del $IP_DNS_ADM dev eth0.2
    ip a del $GATEWAY_6 dev eth0
    echo "[OK]"

    # Les routes doivent revenir à la normale
    echo -n "Restauration de la route par défaut"
    ip r replace default via $GATEWAY_CRANS dev eth0
    echo "[OK]"
}

stop_IPv6()
{
    # Le routeur principal doit pouvoir reparler à SixXS, et on arrête de
    # se faire passer pour lui
    echo -n "Phase 1: Arrêt des services IPv6 ... "
    systemctl stop aiccu radvd
    ip r add default via $GATEWAY_6 dev crans
    echo "[OK]"
}

stop_IPv4()
{
    # Ne plus embrouiller le routeur de l'ENS concernant les routes vers la zone Crans
    echo -n "Phase 2: Arrêt des services IPv4 ... "
    systemctl stop quagga
    echo "[OK]"
}

read -p "Le serveur va voler toutes les adresses IP. Es-tu sur de ce que tu fais ? " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    case "$1" in
        start)
            echo "Démarrage du routeur de secours"
            applying_for_gateway
            setup_IPv4
            setup_IPv6
            start_firewall
            take_radius
            take_dhcp
            take_dns
            ;;
        stop)
            echo "Arrêt du routeur de secours"
            stop IPv6
            stop_IPv4
            being_fired
            ;;
        *)
            echo "42"
            echo "Essaye plutôt start/stop."
            ;;
    esac
fi
exit 0





