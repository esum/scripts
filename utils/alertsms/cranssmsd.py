#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

"""
    Corps principal pour le daemon du service SMS du Crans
"""

from __future__ import print_function

import os
import pwd
import grp
import json
import signal

import daemon
from daemon import pidlockfile
import gammu
import gammu.smsd

from cranslib import clogger
from utils.alertsms import config, AMQPChannel

if __name__ == "__main__":

    ## Paramètres

    # Concernant le daemon cranssmsd
    PIDFILE = '/var/run/cranssmsd/cranssmsd.pid'
    USER = 'gammu'
    GROUP = 'gammu'

    os.umask(0o037)

    ## Initialisation du logger
    logger = clogger.CLogger("cranssmsd")

    ## Initialisation du contexte du daemon

    context = daemon.DaemonContext()

    # Paramètres de fichiers
    context.files_preserve = [logger.c_file_handler.stream.fileno()]
    context.umask = 0o037
    context.pidfile = pidlockfile.PIDLockFile(PIDFILE)

    # Handlers pour les signaux
    context.signal_map = {
        signal.SIGTERM : 'terminate',
        signal.SIGHUP : None,
    }

    # UID et GID du daemon
    context.uid = pwd.getpwnam(USER).pw_uid
    context.gid = grp.getgrnam(GROUP).gr_gid

    # Le daemon ne dumpe pas de core en cas d'arrêt anormal
    context.prevent_core = True

    # Descripteurs de fichiers pour les entrées et sorties standards
    context.stdin = None
    context.stdout = logger.c_file_handler.stream
    context.stderr = logger.c_file_handler.stream

    ## Fonctions appelées par le daemon
    def obfuscate(number):
        return number[:2] + ("X" * len(number[2:-2:1])) + number[-2:]

    def deserialize_messages(payload):
        data = json.loads(payload)
        logger.info(u"Nouveau messages reçu:")
        logger.info(u"\tPour: %s" % ",".join(map(obfuscate, data['destinataires'])))
        logger.info(u"\tContenu : %s" % data['message'])

        sms_list = []

        for number in data['destinataires']:
            sms_list.append({
                'SMSC' : config.SMSC_DEFAULT,
                'Number' : number,
                'Text' : data['message'],
            })

        return sms_list

    def send_message(channel, method, properties, body):
        try:
            gammu.smsd.SMSD(config.SMSD_CONF).InjectSMS(deserialize_messages(body))
            logger.info("Message(s) envoyé(s) !")
        except ValueError:
            logger.error("Impossible de désérialiser le message")
        except KeyError:
            logger.error("Message mal construit")

    def wait_for_messages():
        AMQPChannel().listen(send_message)

    ## Lancement du daemon
    with context:
        wait_for_messages()
