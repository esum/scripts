#-*- coding: utf-8 -*-

import json

import pika

import utils.alertsms.config

class AMQPChannel(object):
    _connection = None
    _channel = None

    def __init__(self, *args, **kwargs):
        self.connect()

    def send(self, data):
        return self._channel.basic_publish(exchange='', routing_key=config.QUEUE, body=data)

    def listen(self, callback, no_ack=True):
        self._channel.basic_consume(callback, queue=config.QUEUE, no_ack=no_ack)
        self._channel.start_consuming()

    @classmethod
    def connect(cls):
        """
            Initialise un canal de communication actif avec le serveur AMQP
        """
        if cls._connection is None:
            cls._connection = pika.BlockingConnection(config.PARAMS)
        elif not cls._connection.is_open:
            cls._connection.connect()

        if cls._channel is None:
            cls._channel = cls._connection.channel()
            cls._channel.queue_declare(config.QUEUE)

    @classmethod
    def close(cls):
        """
            Stoppe la communication avec le serveur AMQP
        """
        if cls._connection is not None and cls._connection.is_open:
            cls._connection.close()
            cls._channel = None


def send(message, destinataires):
    """
        Fonction permettant l'envoi d'un SMS à 1 ou plusieurs destinataires.
        *   'message' est une chaïne de caractères contenant le message à envoyer
        *   'destinataires' est une liste de numéros de téléphones.
    """
    if not isinstance(message, (str, unicode)):
        raise ValueError("message doit être un bytestring ou une chaîne Unicode")
    if not isinstance(destinataires, (list, set, tuple)):
        destinataires = list(destinataires)

    # Sérialisation des données
    data = json.dumps({'destinataires' : destinataires, 'message' : message})

    # Envoi des messages au serveur AMQP
    AMQPChannel().send(data)
