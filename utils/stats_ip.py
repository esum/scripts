#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-


from lc_ldap import shortcuts
from config import NETs
from iptools import AddrInNet

ldap = shortcuts.lc_ldap_readonly()


ips = [ unicode(x['ipHostNumber'][0]) for x in ldap.search(u'(ipHostNumber=*)',sizelimit=10000)]

for n in NETs:

    nb_total = 0
    for net in NETs[n]:
        nb_total += 2 ** ( 32 - int( net.split('/')[1] ) )

    nb_occupe = len( [ ip for ip in ips if AddrInNet( ip, NETs[n] ) ] )

    print '%12s %2s%% (%s/%s)' % (n, 100*nb_occupe/nb_total, nb_occupe, nb_total)
