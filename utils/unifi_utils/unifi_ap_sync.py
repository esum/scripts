#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
#
# Copyright 2017 GPLv2
#
# Gabriel Detraz <detraz@crans.org>
#
# Ce script met à jour les noms des ap dans la bdd unifi
# et fait d'autres choses utiles
# A utiliser sur unifi en localhost

from pymongo import MongoClient
from lc_ldap import shortcuts

# Connexion mongodb
client = MongoClient("mongodb://localhost:27117")

# Connexion lc_ldap
ldap = shortcuts.lc_ldap_admin()

db = client.ace

device = db['device']

bornes = ldap.search(u'objectClass=BorneWifi', mode='rw')

def set_bornes_names(liste_bornes):
    """Met à jour les noms des bornes dans la bdd du controleur"""
    for borne in liste_bornes:
        device.find_one_and_update({'ip': str(borne['ipHostNumber'][0])}, {'$set': {'name': borne['host'][0].split('.')[0]}})
    return

def set_untag_vlan(liste_bornes):
    """Regarde quelles bornes sont gérées avec le controleur
    et set la variable untagvlan à 3 dans ldap pour bien envoyer le vlan
    wifi untag sur les ports en question"""
    for borne in liste_bornes:
        if device.find_one({'ip': str(borne['ipHostNumber'][0])}):
            with borne as edit_borne:
                edit_borne['untagvlan'] = u"3"
                edit_borne.history_gen()
                edit_borne.save()
    return

if __name__ == '__main__':
    set_bornes_names(bornes)
    set_untag_vlan(bornes)
