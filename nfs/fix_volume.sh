#!/bin/bash
# Script à exécuter quand un volume est endommagé.
# Gare, il peut foutre la merde.

letter=$1

fail(){
    echo $1 && exit 1
}


echo "Unexporting /home-adh${letter}"
exportfs -u 10.231.136.0/24:/home-adh/${letter} || fail "Failed to unexport /home-adh/${letter}"

echo "Umounting /home-adh/${letter}"
umount /home-adh/${letter} || fail "Failed to umount /home-adh/${letter}"

echo "Checking /dev/iscsi_home_${letter}_part1 in 3 seconds..."
sleep 3
fsck /dev/iscsi_home_${letter}_part1

echo "Mounting back /home-adh/${letter}"
mount /home-adh/${letter} || fail "Failed to mount back /home-adh/${letter}"

echo "Re-exporting /home-adh/${letter}"
exportfs -o rw,async,wdelay,nohide,no_root_squash,no_subtree_check 10.231.136.0/24:/home-adh/${letter} || fail "Failed to reexport /home-adh/${letter}"

echo "Re-enabling quotas on /home-adh/${letter}"
quotaon -u /home-adh/${letter}
