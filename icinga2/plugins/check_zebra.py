#!/usr/scripts/python.sh
# -*- coding: utf-8 -*-

"""
    Vérifie l'état interne de Zebra et récupère les routes enregistrées
"""

from __future__ import unicode_literals, print_function

import os.path
import re
import itertools
import argparse
import telnetlib

import nagiosplugin
from nagiosplugin.state import Ok, Warn, Critical

from gestion import secrets_new

parser = argparse.ArgumentParser()
parser.add_argument('-H', '--hostname', type=unicode, required=True, help="Hostname or IP address")
parser.add_argument('-p', '--port', type=int, default=2601, help="Port used to talk to zebra")
parser.add_argument('-k4', '--kernel-ipv4', action='store_true', help='Consider IPv4 Kernel routes')
parser.add_argument('-k6', '--kernel-ipv6', action='store_true', help='Consider IPv6 Kernel routes')
parser.add_argument('-b4', '--ebgp-ipv4', action='store_true', help='Consider IPv4 eBGP routes')
parser.add_argument('-b6', '--ebgp-ipv6', action='store_true', help='Consider IPv6 eBGP routes')
parser.add_argument('-o4', '--ospf-ipv4', action='store_true', help='Consider IPv4 OSPF routes')
parser.add_argument('-o6', '--ospf-ipv6', action='store_true', help='Consider IPv6 OSPF routes')
args = parser.parse_args()


IPPROTOS = [4, 6]

RTPROTOS = {
    'kern' : 'kernel',
    'ebgp' : 'eBGP',
    'ospf' : 'OSPF',
}

KERN_RE = re.compile(b"kernel\s+\d+\s+(?P<routes>\d+)")
EBGP_RE = re.compile(b"ebgp\s+\d+\s+(?P<routes>\d+)")
OSPF_RE = re.compile(b"ospf\s+\d+\s+(?P<routes>\d+)")

FAILOVER_IDLE = not os.path.exists('/etc/keepalived/BACKUP_ACTIVE')

class Zebra(nagiosplugin.Resource):
    """
        Récupère les données depuis le daemon Zebra et les traite
    """
    def __init__(self, hostname, port, kern4, kern6, ebgp4, ebgp6, ospf4, ospf6):
        self.hostname = hostname
        self.port = port
        self.kern4 = kern4
        self.kern6 = kern6
        self.ebgp4 = ebgp4
        self.ebgp6 = ebgp6
        self.ospf4 = ospf4
        self.ospf6 = ospf6
        self.__conn = None

    def get_zebra_data(self):
        ZEBRA_PROMPT = b"zebra> "
        self.__conn = telnetlib.Telnet(self.hostname, self.port)
        self.__conn.expect([b"Password: "], 2)
        self.__conn.write(secrets_new.get('zebra_password').encode('utf-8'))
        self.__conn.write(b"\r\n")
        self.__conn.expect([ZEBRA_PROMPT], 2)
        self.__conn.write(b"show ip route summary\n")
        self.__conn.write(b"show ipv6 route summary\n")
        self.__conn.write(b"exit\n")
        return dict(zip((4, 6), self.__conn.read_all().split(ZEBRA_PROMPT)[1:3]))

    def _get_routes(self, match):
        if match:
            return int(match.groupdict()['routes'])
        return 0

    def get_kern(self, results):
        kern_routes = {version : 0 for version in IPPROTOS}
        for version in IPPROTOS:
            match = KERN_RE.search(results[version])
            kern_routes[version] += self._get_routes(match)
        return kern_routes

    def get_ebgp(self, results):
        ebgp_routes = {version : 0 for version in IPPROTOS}
        for version in IPPROTOS:
            match = EBGP_RE.search(results[version])
            ebgp_routes[version] += self._get_routes(match)
        return ebgp_routes

    def get_ospf(self, results):
        ospf_routes = {version : 0 for version in IPPROTOS}
        for version in IPPROTOS:
            match = OSPF_RE.search(results[version])
            ospf_routes[version] += self._get_routes(match)
        return ospf_routes

    def probe(self):
        try:
            data = self.get_zebra_data()
        except telnetlib.socket.error:
            yield nagiosplugin.Metric('no_daemon', None, context='no_daemon')
            raise StopIteration
        for rtproto, ipproto in itertools.product(RTPROTOS, IPPROTOS):
            routes = getattr(self, 'get_%s' % rtproto)(data)
            yield nagiosplugin.Metric(
                'ipv%d_%s' % (ipproto, rtproto),
                routes[ipproto],
                min=1,
                context='%s%d_routes' % (rtproto, ipproto) if getattr(self, "%s%d" % (rtproto, ipproto)) else 'dummy_routes',
            )

class ZebraErrorContext(nagiosplugin.ScalarContext):
    """
        Met en contexte les informations lorsque Zebra est injoignable
    """
    def describe(self, metric):
        if FAILOVER_IDLE:
            return "Zebra inactif (Routeur de secours)"
        else:
            return "Impossible de contacter Zebra"

    def evaluate(self, metric, resource):
        if FAILOVER_IDLE:
            return nagiosplugin.Result(Ok, metric=metric)
        else:
            return nagiosplugin.Result(Critical, metric=metric)

    def performance(self, metric, resource):
        return None

class ZebraContext(nagiosplugin.ScalarContext):
    """
        Met en contexte les informations sur les routes de Zebra
    """
    def evaluate(self, metric, resource):
        if not self.critical.match(metric.value):
            return nagiosplugin.Result(Critical, metric=metric)
        elif not self.warning.match(metric.value):
            return nagiosplugin.Result(Warn, metric=metric)
        else:
            return super(ZebraContext, self).evaluate(metric, resource)

class ZebraSummary(nagiosplugin.Summary):
    """
        Affiche un résumé plus lisible pour les résultats renvoyés par Zebra.
    """
    def __init__(self, kern4, kern6, ebgp4, ebgp6, ospf4, ospf6):
        self.kern4 = kern4
        self.kern6 = kern6
        self.ebgp4 = ebgp4
        self.ebgp6 = ebgp6
        self.ospf4 = ospf4
        self.ospf6 = ospf6

    def empty(self):
        return "Aucune route"

    def _print(self, results):
        if results.first_significant.context.name == 'no_daemon':
            return str(results.first_significant)
        routes = {'all' : sum(r.metric.value for r in results)}
        if not results.first_significant.state == Ok:
            template = "{all} routes, %s" % "".join(str(r) for r in results.most_significant)
        else:
            template = "{all} routes"
        for rtproto in RTPROTOS:
            routes['%s_all' % rtproto] = sum(r.metric.value for r in results if rtproto in r.metric.name)
            routes['%s_ipv4' % rtproto] = results["ipv4_%s" % rtproto].metric.value
            routes['%s_ipv6' % rtproto] = results["ipv6_%s" % rtproto].metric.value
            template += "\n{%s_all} %s routes" % (rtproto, RTPROTOS[rtproto])
            template += " ({%s_ipv4} IPv4, {%s_ipv6} IPv6)" % (rtproto, rtproto)
        return template.format(**routes)

    ok = _print
    problem = _print

@nagiosplugin.guarded
def main():
    check = nagiosplugin.Check(
        Zebra(args.hostname, args.port, args.kernel_ipv4, args.kernel_ipv6, args.ebgp_ipv4, args.ebgp_ipv6, args.ospf_ipv4, args.ospf_ipv6),
        ZebraContext('kern4_routes', '1:', '1:', fmt_metric='{value} Kernel IPv4 routes'),
        ZebraContext('kern6_routes', '1:', '1:', fmt_metric='{value} Kernel IPv6 routes'),
        ZebraContext('ebgp4_routes', '1:', '1:', fmt_metric='{value} eBGP IPv4 routes'),
        ZebraContext('ebgp6_routes', '1:', '1:', fmt_metric='{value} eBGP IPv6 routes'),
        ZebraContext('ospf4_routes', '1:', '1:', fmt_metric='{value} OSPF IPv4 routes'),
        ZebraContext('ospf6_routes', '1:', '1:', fmt_metric='{value} OSPF IPv6 routes'),
        ZebraContext('dummy_routes', '0:', '0:', fmt_metric='{value} routes'),
        ZebraErrorContext('no_daemon', '0:', '0:'),
        ZebraSummary(args.kernel_ipv4, args.kernel_ipv6, args.ebgp_ipv4, args.ebgp_ipv6, args.ospf_ipv4, args.ospf_ipv6),
    )
    check.main(0)

if __name__ == "__main__":
    main()
