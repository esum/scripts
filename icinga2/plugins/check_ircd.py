#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function

import os
import argparse
import telnetlib

import nagiosplugin


class IRCdSummary(nagiosplugin.Summary):
    """
        Affiche un résumé plus fourni que le résumé par défaut pour le check
        du serveur IRC
    """
    def ok(self, results):
        return "%s canaux / %s utilisateurs" % (results['channels'].metric, results['users'].metric)

class IRCd(nagiosplugin.Resource):
    """
        Renvoie des informations sur le serveur IRC
    """
    items = ['num_channels', 'num_users']

    def __init__(self, hostname, port):
        self.hostname = hostname
        self.port = port
        self.__connection = None

    def connect(self):
        self.__connection = telnetlib.Telnet(self.hostname, self.port)
        self.__connection.write(b"NICK ircd%d\r\n" % os.getpid())
        self.__connection.write(b"USER nagios 8 * :\r\n")
        _, challenge, _ = self.__connection.expect([b"PING :(?P<challenge>.+)\r\n"], 2)
        self.__connection.read_until(b"now.\r\n")
        self.__connection.write(b"PONG " + challenge.groupdict()['challenge'] + b"\r\n")
        self.__connection.read_until(b"End of message of the day.\r\n")

    def get_num_channels(self):
        _, channels, _ = self.__connection.expect([b"(?P<channels>\d+) :channels formed"], 5)
        return nagiosplugin.Metric(
            "channels",
            int(channels.groupdict()['channels']),
            min=0,
            context='channels',
        )

    def get_num_users(self):
        _, users, _ = self.__connection.expect([b"Current Global Users: (?P<users>\d+)"], 5)
        return nagiosplugin.Metric(
            "users",
            int(users.groupdict()['users']),
            min=0,
            context='users',
        )

    def quit(self):
        self.__connection.write(b"QUIT\r\n")

    def probe(self):
        self.connect()
        for item in self.items:
            yield getattr(self, "get_%s" % item)()
        self.quit()


@nagiosplugin.guarded
def main():
    """
        Fonction principale interrogeant le serveur IRC
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-H', '--hostname', metavar='host', type=unicode, help="Hostname or IP address", default="localhost")
    parser.add_argument('-p', '--port', metavar='port', type=int, help="Remote server port", default=6667)
    parser.add_argument('-W', '--user-warning', metavar='level', type=int, help="Warning user level", default=150)
    parser.add_argument('-C', '--user-critical', metavar='level', type=int, help="Critical user level", default=200)
    parser.add_argument('-w', '--chan-warning', metavar='level', type=int, help="Warning channel level", default=150)
    parser.add_argument('-c', '--chan-critical', metavar='level', type=int, help="Critical channel level", default=200)

    args = parser.parse_args()

    check = nagiosplugin.Check(
        IRCd(args.hostname, args.port),
        nagiosplugin.ScalarContext(
            'channels', args.chan_warning, args.chan_critical, fmt_metric='{value} canaux'
        ),
        nagiosplugin.ScalarContext(
            'users', args.user_warning, args.user_critical, fmt_metric='{value} utilisateurs'
        ),
        IRCdSummary(),
    )
    check.main()


if __name__ == "__main__":
    main()
