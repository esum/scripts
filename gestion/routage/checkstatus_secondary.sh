#!/bin/bash

# Renvoie un si le fichier BACKUP_ACTIVE est présent
# Verifie le statut de quagga, si quagga est down, tente de le relancer
# puis passe en fault

if [ -f /etc/keepalived/BACKUP_ACTIVE ]; then
    if systemctl is-active quagga; then
        exit 1
    else
        systemctl restart quagga
        if systemctl is-active quagga; then
            exit 1
        else
            exit 0
        fi
    fi
else
    exit 0
fi
