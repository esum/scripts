# -*- mode: python; coding: utf-8 -*-

import subprocess
import tempfile
import atexit

from gestion import secrets_new as secrets

_options = ['PasswordAuthentication=no', 'ConnectTimeout=1', 'VerifyHostKeyDNS=no',
        'BatchMode=yes', 'ServerAliveInterval=5', 'ServerAliveCountMax=1']
_args = ["ssh", "-4", ]

def build_args(host):
    if not 'adm.crans.org' in host:
        host=host + '.adm.crans.org'
    args = list(_args)
    for opt in _options:
        args.append('-o')
        args.append(opt)

    # Ajoute clef privée
    key = tempfile.NamedTemporaryFile()
    atexit.register(key.close)
    key.write(secrets.get('trigger-generate'))
    key.flush()
    args.extend(['-i', key.name])

    args.extend(["rpcssh@%s" % host, "generate"])
    return args

def trigger_generate(host, background=False):
    args = build_args(host)
    if background:
        subprocess.Popen(args)
    else:
        p=subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        if err:
            raise Exception(err)
        return out

def trigger_generate_cochon(host):
    """Ceci est une fonction crade qui permet de se débarraser du process enfant
    que l'on aurait laissé en arrière plan"""
    args = build_args(host)
    p = subprocess.Popen(['/bin/bash'],
        stdin=subprocess.PIPE, )
    p.communicate(' '.join(args) + ' &> /dev/null &')
