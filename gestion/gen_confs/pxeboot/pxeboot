#!/bin/bash -e
#
# pxeboot.sh: création d'un répertoire TFTP de boot par PXE
# Copyright (C) 2008, Nicolas Dandrimont <Nicolas.Dandrimont@crans.org>
#
# Utilisation : pxeboot.sh IP_SRV_TFTP

# Définitions communes

. config

umount $TFTPROOT/livecd/ubuntu/* || true;
rm -rf $TFTPROOT || exit 1

####################################
# Vérifications
####################################
for type in $UBUNTU_LIVE_TYPE; do
	for dist in $UBUNTU_LIVE; do
	    for arch in $UBUNTU_LIVE_ARCHS; do
	        if ! test -e $ISODIR/ubuntu/$type-$dist-desktop-$arch.iso 
	        then echo "$ISODIR/ubuntu/$type-$dist-desktop-$arch.iso n'existe pas" && sleep 3;
	        fi
	    done
	done
done

mkdir -vp $TMPDIR
#mkdir -vp $TFTPROOT
#mkdir -vp $TFTPROOT/boot-screens
#mkdir -vp $TFTPROOT/pxelinux.cfg
#
##############################################
# Copie des fichiers de base pour le PXE
cp -ra $SKELETON $TFTPROOT
for system in $SYSTEMS; do
    cd $TFTPROOT/$system/
    ln -s ../pxelinux.cfg/ pxelinux.cfg
    ln -s ../boot-screens/ boot-screens
done
# On redémarre de tftp
/etc/init.d/tftpd-hpa restart
##############################################


# Génération du fichier de configuration PXELINUX pour BIOS

for system in $SYSTEMS; do
    cat > $TFTPROOT/pxelinux.cfg/$system << EOF
PATH $system/
UI vesamenu.c32
INCLUDE /boot-screens/menu.cfg
EOF
done

cd $TFTPROOT/pxelinux.cfg/
ln -s legacy-bios bios

# Menu PXE commun à tous les systèmes

cat > $TFTPROOT/boot-screens/menu.cfg << EOF
MENU HSHIFT 20
MENU WIDTH 49

MENU TITLE Install-Party Cr@ns
MENU BACKGROUND boot-screens/splash.png
MENU VSHIFT 18
MENU ROWS 5
MENU TABMSGROW 16
MENU TIMEOUTROW 17
MENU TABMSG Press ENTER to boot or TAB to edit a menu entry
MENU AUTOBOOT Starting Local System in # seconds

LABEL bootlocal
    MENU LABEL ^Boot from local disk
        MENU DEFAULT
        LOCALBOOT 0
        TIMEOUT 200           #timeout which is displayed, Wait 10 seconds unless the user types somethin
        TOTALTIMEOUT 1200     #timeout which executes the default definitely, always boot after 2 minutes

EOF

###########################
#         DEBIAN          #
###########################

for dist in $DEBIAN_DISTS; do
    for arch in $DEBIAN_ARCHS; do
        wget $WGETOPT -c $DEBIAN_FTP/$dist/main/installer-$arch/current/images/netboot/netboot.tar.gz -O $TMPDIR/netboot-debian-$dist-$arch.tar.gz
        mkdir -p $TMPDIR/netboot-debian-$dist-$arch/
        tar zxf $TMPDIR/netboot-debian-$dist-$arch.tar.gz -C $TMPDIR/netboot-debian-$dist-$arch/
        mkdir -p $TFTPROOT/debian-$dist/$arch
        cp $TMPDIR/netboot-debian-$dist-$arch/debian-installer/$arch/initrd.gz $TFTPROOT/debian-$dist/$arch
        cp $TMPDIR/netboot-debian-$dist-$arch/debian-installer/$arch/linux $TFTPROOT/debian-$dist/$arch
        #~ wget $WGETOPT -c $DEBIAN_FTP/$dist/main/installer-$arch/current/images/netboot/gtk/netboot.tar.gz -O $TMPDIR/netboot-debian-gtk-$dist-$arch.tar.gz
        #~ mkdir -p $TMPDIR/netboot-debian-gtk-$dist-$arch/
        #~ tar zxf $TMPDIR/netboot-debian-gtk-$dist-$arch.tar.gz -C $TMPDIR/netboot-debian-gtk-$dist-$arch/
        #~ mkdir -p $TFTPROOT/debian-gtk-$dist/$arch
        #~ cp $TMPDIR/netboot-debian-gtk-$dist-$arch/debian-installer/$arch/initrd.gz $TFTPROOT/debian-gtk-$dist/$arch
        #~ cp $TMPDIR/netboot-debian-gtk-$dist-$arch/debian-installer/$arch/linux $TFTPROOT/debian-gtk-$dist/$arch
        #wget $WGETOPT -c $DEBIAN_FTP/$dist/main/installer-kfreebsd-$arch/current/images/netboot/netboot.tar.gz -O $TMPDIR/netboot-debian-kfreebsd-$dist-$arch.tar.gz
        #mkdir -p $TMPDIR/netboot-debian-$dist-kfreebsd-$arch/
        #tar zxf $TMPDIR/netboot-debian-kfreebsd-$dist-$arch.tar.gz -C $TMPDIR/netboot-debian-$dist-kfreebsd-$arch/
        #mkdir -p $TFTPROOT/debian-$dist/kfreebsd-$arch/
        #        cp -r $TMPDIR/netboot-debian-$dist-kfreebsd-$arch/* $TFTPROOT/debian-$dist/kfreebsd-$arch/
    done
    # On génère un lien symbolique depuis les dossiers des différents systèmes vers les images
    for system in $SYSTEMS; do
        cd $TFTPROOT/$system/
        ln -s ../debian-$dist/ debian-$dist
    done
done

# Ajout des options PXE pour Debian (Legacy)

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
MENU BEGIN debian
      MENU TITLE Debian
      LABEL mainmenu
              MENU LABEL ^Back..
              MENU exit
EOF

for dist in $DEBIAN_DISTS; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
      MENU BEGIN debian-$dist
              MENU TITLE Debian $dist
              LABEL mainmenu
                      MENU LABEL ^Back..
                      MENU exit
EOF
for arch in $DEBIAN_ARCHS; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
              MENU BEGIN debian-$dist-$arch
                      MENU TITLE Debian $dist $arch
                      LABEL mainmenu
                              MENU LABEL ^Back..
                              MENU exit
                      LABEL install
                              MENU DEFAULT
                              KERNEL debian-$dist/$arch/linux
                              APPEND vga=normal initrd=debian-$dist/$arch/initrd.gz --
                      LABEL expert
                              KERNEL debian-$dist/$arch/linux
                              APPEND priority=low vga=normal initrd=debian-$dist/$arch/initrd.gz --
                      LABEL rescue
                              KERNEL debian-$dist/$arch/linux
                              APPEND vga=normal initrd=debian-$dist/$arch/initrd.gz rescue/enable=true --
                      LABEL auto
                              KERNEL debian-$dist/$arch/linux
                              APPEND auto=true priority=critical vga=normal initrd=debian-$dist/$arch/initrd.gz --
              MENU END
EOF
done
#for arch in $DEBIAN_ARCHS; do
#cat >> $TFTPROOT/boot-screens/menu.cfg <<EOF
#              LABEL Debian $dist kfreebsd-$arch
#                    kernel debian-$dist/kfreebsd-$arch/grub2pxe
#EOF
#done
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
      MENU END

EOF
#~ cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
      #~ menu begin debian-gtk-$dist
              #~ menu title Debian GTK $dist
              #~ label mainmenu
                      #~ menu label ^Back..
                      #~ menu exit
#~ EOF
#~ for arch in $DEBIAN_ARCHS; do
#~ cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
              #~ menu begin debian-gtk-$dist-$arch
                      #~ menu title Debian GTK $dist $arch
                      #~ label mainmenu
                              #~ menu label ^Back..
                              #~ menu exit
                      #~ DEFAULT install
                      #~ LABEL install
                              #~ kernel debian-gtk-$dist/$arch/linux
                              #~ append vga=normal initrd=debian-gtk-$dist/$arch/initrd.gz --
                      #~ LABEL expert
                              #~ kernel debian-gtk-$dist/$arch/linux
                              #~ append priority=low vga=normal initrd=debian-gtk-$dist/$arch/initrd.gz --
                      #~ LABEL rescue
                              #~ kernel debian-gtk-$dist/$arch/linux
                              #~ append vga=normal initrd=debian-gtk-$dist/$arch/initrd.gz rescue/enable=true --
                      #~ LABEL auto
                              #~ kernel debian-gtk-$dist/$arch/linux
                              #~ append auto=true priority=critical vga=normal initrd=debian-gtk-$dist/$arch/initrd.gz --
              #~ menu end
#~ EOF
#~ done
#~ cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
      #~ menu end

#~ EOF
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
MENU END
EOF

###########################
#       fin DEBIAN        #
###########################


###########################
#         UBUNTU          #
###########################

for dist in $UBUNTU_DISTS; do
    for arch in $UBUNTU_ARCHS; do
        mkdir -p $UBUNTU_FTP/$dist/main/installer-$arch/current/images/netboot
        wget $WGETOPT -c $UBUNTU_FTP/$dist/main/installer-$arch/current/images/netboot/netboot.tar.gz -O $TMPDIR/netboot-ubuntu-$dist-$arch.tar.gz
        mkdir -p $TMPDIR/netboot-ubuntu-$dist-$arch/
        tar zxf $TMPDIR/netboot-ubuntu-$dist-$arch.tar.gz -C $TMPDIR/netboot-ubuntu-$dist-$arch/
        mkdir -p $TFTPROOT/ubuntu-$dist/$arch
        cp $TMPDIR/netboot-ubuntu-$dist-$arch/ubuntu-installer/$arch/initrd.gz $TFTPROOT/ubuntu-$dist/$arch
        cp $TMPDIR/netboot-ubuntu-$dist-$arch/ubuntu-installer/$arch/linux $TFTPROOT/ubuntu-$dist/$arch
    done
    # On génère un lien symbolique depuis les dossiers des différents systèmes vers les images
    for system in $SYSTEMS; do
        cd $TFTPROOT/$system/
        ln -s ../ubuntu-$dist/ ubuntu-$dist
    done
done

for type in $UBUNTU_LIVE_TYPE; do
    for dist in $UBUNTU_LIVE; do
        for arch in $UBUNTU_LIVE_ARCHS; do
            if test -e $ISODIR/ubuntu/$type-$dist-desktop-$arch.iso; then
    	        mkdir -p $TFTPROOT/livecd/ubuntu/$type-$dist-$arch
    	        mount -o loop $ISODIR/ubuntu/$type-$dist-desktop-$arch.iso $TFTPROOT/livecd/ubuntu/$type-$dist-$arch
            fi
    	done
    done
done

for system in $SYSTEMS; do
    cd $TFTPROOT/$system/
    ln -s ../livecd/ livecd
done

/etc/init.d/nfs-kernel-server start

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
MENU BEGIN ubuntu-livecd
      MENU TITLE Ubuntu LiveCd
      LABEL mainmenu
              MENU LABEL ^Back..
              MENU exit
EOF
for type in $UBUNTU_LIVE_TYPE; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
       MENU BEGIN ubuntu-livecd-$type
             MENU TITLE $type
             LABEL mainmenu
                   MENU LABEL ^Back..
                   MENU exit
EOF
for dist in $UBUNTU_LIVE; do
for arch in $UBUNTU_LIVE_ARCHS; do
if test -e $ISODIR/ubuntu/$type-$dist-desktop-$arch.iso; then
if [ -f $TFTPROOT/livecd/ubuntu/$type-$dist-$arch/casper/vmlinuz ]; then
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
                      LABEL $type Livecd $dist $arch
                      KERNEL /livecd/ubuntu/$type-$dist-$arch/casper/vmlinuz
                      APPEND root=/dev/nfs boot=casper netboot=nfs nfsroot=$OWN_IP:$TFTPROOT/livecd/ubuntu/$type-$dist-$arch initrd=livecd/ubuntu/$type-$dist-$arch/casper/initrd.lz --

EOF
else
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
                      LABEL $type Livecd $dist $arch
                      KERNEL /livecd/ubuntu/$type-$dist-$arch/casper/vmlinuz.efi
                      APPEND root=/dev/nfs boot=casper netboot=nfs nfsroot=$OWN_IP:$TFTPROOT/livecd/ubuntu/$type-$dist-$arch initrd=livecd/ubuntu/$type-$dist-$arch/casper/initrd.lz --

EOF
fi
fi
done
done
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
MENU END
EOF
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
MENU END
EOF

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
MENU BEGIN ubuntu
      MENU TITLE Ubuntu
      LABEL mainmenu
              MENU LABEL ^Back..
              MENU exit
EOF

for dist in $UBUNTU_DISTS; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
      MENU BEGIN ubuntu-$dist
              MENU TITLE Ubuntu $dist
              LABEL mainmenu
                      MENU LABEL ^Back..
                      MENU exit
EOF
for arch in $UBUNTU_ARCHS; do
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
              MENU BEGIN ubuntu-$dist-$arch
                      MENU TITLE Ubuntu $dist $arch
                      LABEL mainmenu
                              MENU LABEL ^Back..
                              MENU exit
                      LABEL install
                              MENU DEFAULT
                              KERNEL ubuntu-$dist/$arch/linux
                              APPEND vga=normal initrd=ubuntu-$dist/$arch/initrd.gz --
                      LABEL expert
                              KERNEL ubuntu-$dist/$arch/linux
                              APPEND priority=low vga=normal initrd=ubuntu-$dist/$arch/initrd.gz --
                      LABEL rescue
                              KERNEL ubuntu-$dist/$arch/linux
                              APPEND vga=normal initrd=ubuntu-$dist/$arch/initrd.gz rescue/enable=true --
                      LABEL auto
                              KERNEL ubuntu-$dist/$arch/linux
                              APPEND auto=true priority=critical vga=normal initrd=ubuntu-$dist/$arch/initrd.gz --
              MENU END
EOF
done
cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
      MENU END

EOF
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
MENU END
EOF

###########################
#       fin UBUNTU        #
###########################

###########################
#          CentOS         #
###########################

for dist in $CENTOS_DISTS; do
    for arch in $CENTOS_ARCHS; do
        mkdir -p $TMPDIR/centos-$dist/$arch/
        wget $WGETOPT -c $CENTOS_FTP/$dist/os/$arch/images/pxeboot/initrd.img -O $TMPDIR/centos-$dist/$arch/initrd.img
        wget $WGETOPT -c $CENTOS_FTP/$dist/os/$arch/images/pxeboot/vmlinuz -O $TMPDIR/centos-$dist/$arch/vmlinuz
    done
    for system in $SYSTEMS; do
        cd $TFTPROOT/$system/
        ln -s ../centos-$dist/ centos-$dist
    done
done
cp -r  $TMPDIR/centos-* $TFTPROOT/

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
MENU BEGIN centos
      MENU TITLE CentOS
      LABEL mainmenu
              MENU LABEL ^Back..
              MENU exit
EOF

for dist in $CENTOS_DISTS; do
    cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    MENU BEGIN centos-$dist
               MENU TITLE CentOS $dist
               LABEL mainmenu
                       MENU LABEL ^Back..
                       MENU exit

EOF

    for arch in $CENTOS_ARCHS; do
        cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
        MENU BEGIN centos-$dist-$arch
                       MENU TITLE CentOS $dist $arch
                       LABEL mainmenu
                               MENU LABEL ^Back..
                               MENU exit
                       LABEL install
                               MENU DEFAULT
                               MENU LABEL ^Install
                               KERNEL centos-$dist/$arch/vmlinuz
                               APPEND ksdevice=eth0 console=tty0 initrd=centos-$dist/$arch/initrd.img ks=http://$OWN_IP/pxe/ks.centos-$dist-$arch.cfg ramdisk_size=8192
        MENU END
EOF
        cat > $KSROOT/ks.centos-$dist-$arch.cfg << EOF
install
url --url $CENTOS_FTP/$dist/os/$arch/
lang en_US.UTF-8
langsupport --default en_US.UTF-8 en_US.UTF-8 fr_FR.UTF-8
keyboard fr-latin1
EOF
    done

    cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    MENU END
EOF
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
MENU END
EOF
###########################
#       fin CentOS        #
###########################

###########################
#         Fedora          #
###########################

for dist in $FEDORA_DISTS; do
    for arch in $FEDORA_ARCHS; do
        mkdir -p $TMPDIR/fedora-$dist/$arch/
                wget $WGETOPT -c $FEDORA_FTP/releases/$dist/Workstation/$arch/os/images/pxeboot/initrd.img -O $TMPDIR/fedora-$dist/$arch/initrd.img ||\
                wget $WGETOPT -c $FEDORA_FTP/releases/$dist/Server/$arch/os/images/pxeboot/initrd.img -O $TMPDIR/fedora-$dist/$arch/initrd.img ||\
        wget $WGETOPT -c $FEDORA_FTP/development/$dist/$arch/os/images/pxeboot/initrd.img -O $TMPDIR/fedora-$dist/$arch/initrd.img
                wget $WGETOPT -c $FEDORA_FTP/releases/$dist/Workstation/$arch/os/images/pxeboot/vmlinuz -O $TMPDIR/fedora-$dist/$arch/vmlinuz ||\
                wget $WGETOPT -c $FEDORA_FTP/releases/$dist/Server/$arch/os/images/pxeboot/vmlinuz -O $TMPDIR/fedora-$dist/$arch/vmlinuz
    done
    for system in $SYSTEMS; do
        cd $TFTPROOT/$system/
        ln -s ../fedora-$dist/ fedora-$dist
    done
done
cp -r $TMPDIR/fedora-* $TFTPROOT/

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
MENU BEGIN fedora
      MENU TITLE Fedora
      LABEL mainmenu
              MENU LABEL ^Back..
              MENU exit
EOF

for dist in $FEDORA_DISTS; do
    cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    MENU BEGIN fedora-$dist
               MENU TITLE Fedora $dist
               LABEL mainmenu
                       MENU LABEL ^Back..
                       MENU exit

EOF

    for arch in $FEDORA_ARCHS; do
        cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
        MENU BEGIN fedora-$dist-$arch
                       MENU TITLE Fedora $dist $arch
                       LABEL mainmenu
                               MENU LABEL ^Back..
                               MENU exit
                       LABEL install
                               MENU DEFAULT
                               MENU LABEL ^Install
                               KERNEL fedora-$dist/$arch/vmlinuz
                               APPEND initrd=fedora-$dist/$arch/initrd.img repo=$FEDORA_FTP/releases/$dist/Fedora/$arch/os/
        MENU END
EOF
        #~ cat > $KSROOT/ks.fedora-$dist-$arch.cfg << EOF
#~ install
#~ url --url $FEDORA_FTP/releases/$dist/Fedora/$arch/os/
#~ EOF
    done

    cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
    MENU END
EOF
done

cat >> $TFTPROOT/boot-screens/menu.cfg << EOF
MENU END
EOF

###########################
#        Fin Fedora       #
###########################



echo "Fini"
