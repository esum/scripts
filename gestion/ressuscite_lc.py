#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

################# ATTENTION #####################
# À ne pas laisser executable par n'importe qui,#
# permet de récupérer un shell vers lc_ldap     #
#################################################


import sys
import pprint
from affich_tools import coul, prompt
from IPython.frontend.terminal.embed import InteractiveShellEmbed
import contextlib
import lc_ldap.shortcuts
import lc_ldap.attributs
import lc_ldap.crans_utils
import lc_ldap.printing

def reset_ip(machine):
    machine.replace_id("rid", lc_ldap.crans_utils.find_rid_plage(machine['rid'][0].value)[0])
    machine['ipHostNumber']=lc_ldap.attributs.ipHostNumber.default
    machine['ip6HostNumber']=lc_ldap.attributs.ip6HostNumber.default

def reset_mid(machine, proprio=None):
    machine.replace_id("mid")
    if proprio:
        machine.dn="mid=%s,aid=%s,%s" % (machine['mid'][0], proprio['aid'][0], machine.dn.split(',',2)[2])
    else:
        print coul("Pas de proprio de fourni, on suppose que l'aid du dn %s est valide" % machine.dn.split(',',2)[1], "jaune")
        machine.dn="mid=%s,%s" % (machine['mid'][0], machine.dn.split(',',1)[1])


def display():
    global coffins
    cmp=0
    for i in coffins:
        print coul(u"Élement %s de coffins :" % cmp, 'gras')
        i.display()
        cmp+=1

class Edit(Exception):
    pass
if __name__ == '__main__':
    if len(sys.argv) >2:
        conn=lc_ldap.shortcuts.lc_ldap_admin()
        try:
            coffins=conn.gravedig(sys.argv[1], sys.argv[2])
            if not coffins:
                print "Nothing to do"
            elif len(coffins)==1:
                print lc_ldap.printing.sprint(coffins[0])
                ret=prompt("Ressuciter ?[y,n,e]", "N")
                if ret.lower() == 'y':
                    with coffins[0] as machine:
                        machine.create()
                if ret.lower() == 'e':
                    raise Edit("Édition manuelle")
            else:
                raise lc_ldap.attributs.UniquenessError("Plus d'un objet machtant %s" % sys.argv[2])
        except (lc_ldap.attributs.UniquenessError, Edit) as error:
            msg=coul("%s, démarrage d'un shell." % error, 'rouge')
            msg+="""
Les objets ressucitables sont dans la variable \033[1;50mcoffins\033[1;0m.
Une connexion admin à la base ldap dans la variable \033[1;50mconn\033[1;0m.
Pour afficher les objects de \033[1;50mcoffins\033[1;0m, vous pouvez utiliser la fonction \033[1;50mdisplay()\033[1;0m.
Pour ressuciter les machines, il y a en plus les fonctions :
 * reset_mid(machine, proprio=None)
 * reset_ip(machine)
"""
            ipshell = InteractiveShellEmbed.instance(banner1 = unicode(msg, 'utf-8'))
            with contextlib.nested(*coffins) as coffins:
                ipshell()
                sys.exit(0)
        except ValueError as error:
            print coul("%s" % error, "rouge")
    else:
        sys.stderr.write("Usage: %s {cimetière} {filtre}\nLe cimetière est dans /home/cimetiere_lc/\n" % sys.argv[0])
