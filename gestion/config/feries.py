#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

""" Pour déclarer les jours fériés.
    Ça pourra servir à débrider ces jours-là. """

import dateutil.easter
import datetime

def holidays(year):
    """Génère la liste des jours fériés pour l'année ``year``.
       Au format :py:module:`datetime.date`."""
    l = []
    l.append(datetime.date(year=year, month=1, day=1)) # 1er janvier : Jour de l'an
    l.append(datetime.date(year=year, month=5, day=1)) # 1er mai : Fête du travail
    l.append(datetime.date(year=year, month=5, day=8)) # 8 mai : Fête de la victoire
    l.append(datetime.date(year=year, month=7, day=14)) # 14 juillet : Fête Nationale
    l.append(datetime.date(year=year, month=8, day=15)) # 15 août : Assomption
    l.append(datetime.date(year=year, month=11, day=1)) # 1er novembre : Toussaint
    l.append(datetime.date(year=year, month=11, day=11)) # 11 novembre : Armistice
    l.append(datetime.date(year=year, month=12, day=25)) # 25 décembre : Noël
    # Pâques et conséquences
    easter = dateutil.easter.easter(year)
    day = datetime.timedelta(days=1)
    l.append(easter + day) # Lundi de Pâques
    l.append(easter + 39 * day) # Jeudi de l'Ascension
    l.append(easter + 50 * day) # Lundi de Pentecôte
    l.sort()
    return l

def is_ferie(day=None):
    """Dit si ``day`` (au format :py:module:`datetime.date` est férié. (Par défaut aujourd'hui)."""
    if day is None:
        day = datetime.date.today()
    return day in holidays(day.year)

if __name__ == "__main__":
    thisyear = datetime.date.today().year
    for date in holidays(thisyear):
        print date.isoformat()
