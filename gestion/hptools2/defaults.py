#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
#
# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
"""Ce sont les variables utiles pour les autres scripts du
module"""

OPERSTATUS = {
    1: 'up',
    2: 'down',
    3: 'testing',
    4: 'unknown',
    5: 'dormant',
    6: 'notPresent',
    7: 'lowerLayerDown',
}

ADMINSTATUS = {
    1: 'up',
    2: 'down',
    3: 'testing',
}

ETHSPEED = {
    'HD': {
        0: '5',
        10: '1',
        100: '2',
        1000: '5',
    },
    'FD': {
        0: '5',
        10: '3',
        100: '4',
        1000: '6',
    },
    'AUTO': {
        0: '5',
        10: '7',
        100: '8',
        1000: '9',
    },
}

REV_ETHSPEED = {
    '1': '10 Mbs Half Duplex',
    '2': '100 Mbs Half Duplex',
    '3': '10 Mbs Full Duplex',
    '4': '100 Mbs Full Duplex',
    '6': '1000 Mbs Full Duplex',
    '5': 'auto',
    '7': '10 Mbs auto',
    '8': '100 Mbs auto',
    '9': '1000 Mbs auto',
}
