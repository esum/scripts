#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

# Auteur : Rémi Oudin
# Licence : GPLv3
# Date : 11/03/2016


import sys
from gestion.affich_tools import cprint
from gestion import mail
import time
import lc_ldap.shortcuts
import lc_ldap.crans_utils as crans_utils
import argparse


def mail_sender(template, From, ldap_filter, recipientfile=None):
    """
    Récupère les mails dans le ``recipientfile`` ou, à défaut, ceux retournés
    par une recherche avec ``ldap_filter`` et leur envoie le mail
    depuis l'adresse ``From``.
    Utilise le ``template`` passé en argument.
    """
    global conn
    mailaddrs = set()
    print From

    if recipientfile:
        # On a fournit un fichier de destinataires
        with open(recipientfile) as f:
            mailaddrs = set([l.strip() for l in f.readlines() if l.strip() != ""])
    else:
        # Récupère les mails des adhérents donnés par le filtre.
        for adh in conn.search(ldap_filter, sizelimit=2000):
            adh_mail = adh.get_mail()
            if adh_mail:
                mailaddrs.add(adh_mail)
            else:
                if not SEND:
                    cprint("No mail known for %s %s" % \
                        (adh["prenom"][0], adh["nom"][0]),
                           'rouge'
                          )

    echecs = []
    if PREV:
        print "Envoi simulé"

    try:
        print "%d destinataires (Ctrl + C pour annuler l'envoi)" %len(mailaddrs)
        raw_input("Envoyer ? (Ret pour envoyer)\n")
    except KeyboardInterrupt:
        cprint("\nEnvoi annulé.", 'rouge')
        exit(1)
    with mail.ServerConnection() as conn_smtp:
        for To in mailaddrs:
            cprint(u"Envoi du mail à %s" % To)
            mailtxt = mail.generate(
                template,
                {'To' : To,
                 'From' : From,
                 'lang_info' : 'English version below',
                }).as_string()
            if PREV:
                print mailtxt
            try:
                if SEND:
                    conn_smtp.sendmail(From, (To,), mailtxt)
                    cprint(" Envoyé !")
                else:
                    cprint(" (simulé)")
            except:
                print sys.exc_info()[:2]
                cprint(u"Erreur lors de l'envoi à %s " % To, "rouge")
                echecs.append(To)
    if not SEND:
        cprint("\n\
/!\ Avant d'envoyer réellement ce mail all, as-tu vérifié que:\n\
    - Les lignes sont bien formattées à 75-80 caractères ?\n\
    - Le texte a été lu et relu ?\n\
    - Il existe une version en anglais ?\n\
    - Le filtre LDAP appliqué est bien le bon ?\n\
    - Il y a bien une signature ?\n",
    'rouge'
    )

    if echecs:
        print "\nIl y a eu des erreurs :"
        print echecs
        print "\n Pensez à générer des notifications de mail invalide !"
        sys.exit(1)


if __name__ == "__main__":
    # Ouvre une connection à la base LDAP
    conn = lc_ldap.shortcuts.lc_ldap_readonly()

    # Le filtre à appliquer pour la recherche
    ldap_filter = u'(&(finAdhesion>=%(date)s)(aid=*))' %\
              {'date': crans_utils.to_generalized_time_format(time.time())}
    #ldap_filter = u'(uid=detraz)'

    # Attention si à True, envoie effectivement le mail
    SEND = False
    # Si à True, affiche une prévision du mail
    PREV = False
    # Filtre par défaut
    parser = argparse.ArgumentParser(
        description="Mail all générique. Prend un template en argument.",
        add_help=True
    )
    parser.add_argument(
        "-f", "--recipientfile",
        help="Un fichier contenant une adresse mail par ligne. Override le filtre LDAP",
        action="store"
    )
    parser.add_argument(
        "-t", "--template",
        help="Un template de mail. Fournir le chemin du dossier principal du mail, relatif à "
        + mail.template_path,
        action="store"
    )
    parser.add_argument(
        "-s", "--sender",
        help="Spécifier un expéditeur particulier. Par défaut respbats@crans.org",
        action="store",
        default="respbats@crans.org"
    )
    exclusive = parser.add_mutually_exclusive_group()
    exclusive.add_argument(
        "--doit",
        help="Lance effectivement le mail",
        action="store_true"
    )
    exclusive.add_argument(
        "-p", "--prev",
        help="Prévisualise le mail à envoyer",
        action="store_true"
    )

    args = parser.parse_args()

    if args.doit:
        SEND = True
    if args.prev:
        PREV = True
    mail_sender(args.template, args.sender, ldap_filter, args.recipientfile)
    sys.exit(0)
