#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

"""
Script de récupération et de mise à jour des clés GPG des nounous
utilisées pour signer l'archive Debian du Crans
"""

from __future__ import print_function, unicode_literals

import os
import sys
import subprocess
from socket import gethostname

import lc_ldap.shortcuts

KEYSERVER = 'keys.gnupg.net'
BASEDIR = '/bcfg2/Cfg/etc/crans/apt-keys/'

def get_nounous():
    """Renvoie la liste des nounous actives"""
    conn = lc_ldap.shortcuts.lc_ldap_readonly()
    return conn.search("(&(gpgFingerprint=*)(droits=nounou))")

def get_nounous_fingerprints():
    """Renvoie les empreintes de clés GPG des nounous"""
    return [n['gpgFingerprint'][0].value.replace(' ', '') for n in get_nounous()]

def refresh_keys():
    """Mets à jour la liste des clés connues depuis un serveur de clés"""
    cmd = ['gpg', '--keyserver', KEYSERVER, '--recv-keys']
    cmd.extend(get_nounous_fingerprints())
    proc = subprocess.Popen(
        cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE
    )
    _, stderr = proc.communicate()
    if stderr:
        print(stderr, file=sys.stderr)

def write_keys():
    """Écrit les clés des nounous fraîchement mises à jour dans le dossier
    BCfg2 spécifié"""
    for user in get_nounous():
        try:
            os.mkdir('%s%s.asc/' % (BASEDIR, user['uid'][0]))
        except OSError:
            pass
        path = '%s%s.asc/%s.asc' % (BASEDIR, user['uid'][0], user['uid'][0])
        cmd = [
            'gpg',
            '--armor',
            '--export-options', 'export-minimal',
            '--export', user['gpgFingerprint'][0].value.replace(' ', ''),
        ]
        proc = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE
        )
        stdout, stderr = proc.communicate()
        if stderr:
            print(stderr, file=sys.stderr)
        if stdout:
            with open(path, 'w') as output:
                output.write(stdout)
                output.close()

if __name__ == '__main__':
    if gethostname() != 'bcfg2':
        print("Doit être lancé sur bcfg2", file=sys.stderr)
        exit(1)
    else:
        refresh_keys()
        write_keys()
