	# -*- coding: utf8 -*-
import os, random, collections

def dir(path):
	l=collections.deque(["%s%s" % (path,i) for i in os.listdir(path)])
	l.rotate(random.randrange(0,len(l)))
	return l
#dico groupe => (Nom => (tag, multicast ip, multicast port, list de source))
# dans les fait, seule la première source est utilisé
multicast={
'Radio': {
	'Armitunes':			('armitunes','239.231.140.162','1234',['http://mp3.fr.armitunes.com:8000/']),
	'Radio Classique':		('classique','239.231.140.163','1234',['http://broadcast.infomaniak.net:80/radioclassique-high.mp3']),
	'France Inter':			('inter','239.231.140.164','1234',['http://direct.franceinter.fr/live/franceinter-midfi.mp3']),
	'France Info':			('info','239.231.140.165','1234',['http://direct.franceinfo.fr/live/franceinfo-midfi.mp3']),
#	'Webradio Chibre':		('chibre','239.231.140.166','1234',['http://webradio.crans.org:8000/chibre.mp3']),
#	'Webradio Clubbing':		('clubbing','239.231.140.167','1234',['http://webradio.crans.org:8000/clubbing.mp3']),
#	'Webradio Rock':		('rock','239.231.140.168','1234',['http://webradio.crans.org:8000/rock.mp3']),
#	'I.ACTIVE DANCE':		('iactive','239.231.140.170', '1234', ['http://serveur.wanastream.com:48700/']),
	'Skyrock':			('skyrock', '239.231.140.171', '1234', ['https://icecast.skyrock.net/s/natio_aac_64k']),
        'Rire et Chanson':		('rireetchanson', '239.231.140.172', '1234', ['http://cdn.nrjaudio.fm/audio1/fr/30401/aac_64.mp3?awparams=companionAds%3Atrue;playerid:']),
	'Europe 1':			('europe1', '239.231.140.173', '1234', ['http://mp3lg3.scdn.arkena.com/10489/europe1.mp3']),
	'Chérie FM':			('cherie_fm', '239.231.140.174', '1234', ['http://185.52.127.163/fr/40024/aac_64.mp3']),
	'France Culture':		('culture', '239.231.140.175', '1234', ['http://direct.franceculture.fr/live/franceculture-midfi.mp3']),
	'BFM Business':				('bfm', '239.231.140.176', '1234', ['http://bfmbusiness.cdn.dvmr.fr/bfmbusiness']),
	'France Musique':		('musique', '239.231.140.177', '1234', ['http://direct.francemusique.fr/live/francemusique-midfi.mp3']),
	'Fun Radio':			('funradio', '239.231.140.178', '1234', ['http://streaming.radio.funradio.fr/fun-1-44-128.mp3']),
	'Nostalgie':			('nostalgie', '239.231.140.179', '1234', ['http://185.52.127.168/fr/40041/aac_64.mp3']),
	'le mouv\'':			('lemouv', '239.231.140.180', '1234', ['http://chai5she.cdn.dvmr.fr/mouv-midfi.mp3']),
	'NRJ':				('nrj', '239.231.140.181', '1234', ['http://cdn.nrjaudio.fm/audio1/fr/40011/aac_64.mp3']),
	'RTS Fm':				('rtsfm', '239.231.140.182', '1234', ['http://stream.rtsfm.com:8000/']),
	'Sud Radio':			('sud_radio', '239.231.140.183', '1234', ['http://start-sud.ice.infomaniak.ch/start-sud-high.mp3']),
	'France Bleu': 			('bleu', '239.231.140.184', '1234', ['http://direct.francebleu.fr/ts/fb1071-midfi.mp3']),
	'RFM':				('rfm', '239.231.140.185', '1234', ['http://rfm-live-mp3-128.scdn.arkena.com/rfm.mp3']),
	'RTL':				('rtl', '239.231.140.186', '1234', ['http://streaming.radio.rtl.fr/rtl-1-44-128']),
	'RTL2':				('rtl2', '239.231.140.187', '1234', ['http://streaming.radio.rtl2.fr/rtl2-1-44-96']),
	
	},
}
for i in range(1, 4):
    multicast['Radio']['BBC Radio %s' % i]=('bbc%s' % i, '239.231.140.19%s' % i, '1234', ['http://bbcmedia.ic.llnwd.net/stream/bbcmedia_radio%d_mf_p' % i ])
i+=1
multicast['Radio']['BBC Radio %s' % i]=('bbc%s' % i, '239.231.140.19%s' % i, '1234', ['http://bbcmedia.ic.llnwd.net/stream/bbcmedia_radio4fm_mf_p'])

# dico groupe => (tag => (nom, ...))
multicast_tag = {}
for groupe, infos in multicast.items():
    info_tags = {}
    for nom, params in infos.items():
        info_tags[params[0]] = (nom, ) + params[1:]
    multicast_tag[groupe] = info_tags
