#!/usr/bin/env python

from config import multicast
import os
vignettes="/usr/scripts/tv/radio/vignettes/"
for (name, value) in multicast['Radio'].items():
    try: os.unlink('/var/www/images/%s.jpg' % value[1])
    except OSError: pass
    try: os.unlink('/var/www/images/%s_petites.jpg' % value[1])
    except OSError: pass
    if os.path.exists("%s%s.jpg" % (vignettes,name)):
        os.symlink("%s%s.jpg" % (vignettes,name), '/var/www/images/%s.jpg' % value[1])
        os.symlink("%s%s.jpg" % (vignettes,name), '/var/www/images/%s_petites.jpg' % value[1])
    else:
        os.symlink('/var/www/sound.jpg', '/var/www/images/%s.jpg' % value[1])
        os.symlink('/var/www/sound_petites.jpg', '/var/www/images/%s_petites.jpg' % value[1])
