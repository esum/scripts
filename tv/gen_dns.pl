#!/usr/bin/perl -w
#
# Description: generation of bind9 configuration for tv.crans.org
# (direct and reverse zones)
#
# Copyright Â© 2009 StÃ©phane Glondu <steph@glondu.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
#

open(DIRECT, "> /etc/bind/generated/db.tv.crans.org");
open(REVERSE, "> /etc/bind/generated/db.239.in-addr.arpa");
open(CHAINES, "< /tmp/chaines_recup_sap.txt");

my $serial = `date +%s`;
chomp($serial);

print DIRECT "\$ORIGIN tv.crans.org.
\$TTL 86400
@ IN SOA mdr.crans.org. root.crans.org. (
$serial   ; numero de serie
21600     ; refresh
3600      ; retry
1209600   ; expire
86400     ; TTL
)

@ IN NS mdr.crans.org.
@ IN NS charybde.crans.org.
@ IN NS freebox.crans.org.

@ IN A 138.231.136.243

";

print REVERSE "\$ORIGIN 239.in-addr.arpa.
\$TTL 86400
@ IN SOA mdr.crans.org. root.crans.org. (
$serial   ; numero de serie
21600     ; refresh
3600      ; retry
1209600   ; expire
86400     ; TTL
)

@\tIN\tNS mdr.crans.org.
@\tIN\tNS charybde.crans.org.
@\tIN\tNS freebox.crans.org.

";

while (<CHAINES>) {
    s/^[^ ]+ //;
    if (/(.*):(.*)$/) {
	my $name = "$1";
	my $ip = "$2";
	$name =~ s/[ .():,"+<>]//g;
	$name = lc($name);
	print DIRECT "$name IN A $ip\n";
	if ($ip =~ /^\d+\.(\d+)\.(\d+)\.(\d+)$/) {
	    print REVERSE "$3.$2.$1 IN PTR $name.tv.crans.org.\n";
	} else {
	    die "mauvaise adresse IP!";
	}
    }
}
close(DIRECT);
close(CHAINES);
close(REVERSE);
system("/etc/init.d/bind9 reload > /dev/null") == 0 or die "erreur lors du rechargement de bind9!";
