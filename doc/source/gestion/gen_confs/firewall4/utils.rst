
utils -- Le pare-feu ipv4, les fonctions de manipulation
========================================================

.. automodule:: gestion.gen_confs.firewall4.utils
   :members:
   :special-members:
