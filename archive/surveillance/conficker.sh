#!/bin/bash

# Script de détection des machines infectées par Conficker
# Copyright (c) 2009 Michel Blockelet <blockelet@crans.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111, USA.


#####
# Ce script est fait pour détecter les machines infectées par Conficker.
# Il fonctionne un peu à la "devinette", il est donc conseillé de vérifier
# sa sortie (il enregistre chaque étape dans un fichier indiqué par
# [> fichier]).
#####
# Options :
#   -h / --help : Affiche les options
#   --mailto [adresse mail] : Envoie un mail avec le diff
#####
# Fonctionnement :
# -Il analyse les logs de Squid à la recherche des requêtes du type :
#   GET http://[une ip quelconque]/search?
#  qui sont du format de requête typique de Conficker.
# -Il récupère les couples [IP ayant émis la requête] [IP dans la requête]
# -Il retrie ces couples et compte le nombre d'occurences
# -Il regarde le reverse DNS de chaque IP se trouvant dans des requêtes
#   suspectes
# -Il ne garde que les IPs n'ayant pas de reverse DNS (ce qui peut être
#   trop restrictif, mais il s'agit d'éviter de compter les vraies
#   requêtes (émises par exemple vers Google), puis regarde quelles
#   IPs source ont émis des requêtes vers ces IP
# -Il finit par afficher ces IPs sources (machines très probablement
#   infectées), et les statistiques de communication entre ces IPs sources
#   et les IPs destination suspectes
#####


# On affiche l'aide si demandée
if [ "$1" == "-h" ] || [ "$1" == "--help" ]
then
cat <<EOF
Script de détection des machines infectées par Conficker
Utilisation :
$0 [OPTIONS] ...

Options :
 -h / --help : Affiche les options
 --mailto [adresse mail] : Envoie un mail avec le diff, ou les stats
        s'il n'y a pas d'anciennes stats
EOF
  exit 0
fi


# Header pour les mails
header() {
cat <<EOF
***** Détection des machines infectées par Conficker *****
Généré par $0,
le `date` sur `hostname`

*** Hôtes probablement infectés ***
`cat compromised`

*** Statistiques ***
> Format :
>  * [Hôte de destination suspect]
>   [Nombre de requêtes] [IP Cr@ns source] [IP destination]

EOF
}


# A changer bien sûr selon les cas  ...
if [ "`hostname`" != "sable" ] && [ "`hostname`" != "titanic" ]
then
  echo "Vous devez exécuter ce script sur sable ou sur titanic !"
  exit 1
fi

# Ce n'est pas toujours la même version de Squid ... super
if [ "`hostname`" == "sable" ]
then
  SQUIDLOG="/var/log/squid3/access.log"
else
  SQUIDLOG="/var/log/squid/access.log"
fi

# On initialise les fichiers utilisés par le script
# (Par défaut, ces fichiers sont laissés pour ne pas avoir à reexécuter le
# script ...)
echo " * Initialisation des fichiers ..."

# On garde les dernières stats si elles existent ...
if [ -f stats ]
then
 mv stats stats.old
fi

FILES="base diff ip_reqip ip_reqip_stats reverse_dns compromised stats"
rm -f $FILES
touch $FILES
chmod go-rwx $FILES

# On regarde les lignes de la forme "...GET http://[une ip]/search?..."
# (requêtes typiques de Conficker)
echo -n " * Recherche des lignes de logs correspondantes ... [> base] Lignes : "
if [ "`whoami`" == "root" ]
then
  egrep "GET http://([[:digit:]]{1,3}\.){3}[[:digit:]]{1,3}/search\?" $SQUIDLOG | tee base | wc -l
else
  sudo egrep "GET http://([[:digit:]]{1,3}\.){3}[[:digit:]]{1,3}/search\?" $SQUIDLOG | tee base | wc -l
fi

# On récupère les IPs dans les lignes
echo -n " * Récupération des IP sources et destinations ... [> ip_reqip] Lignes : "
cat base | sed 's/^.* \(.*\) TCP.* http:\/\/\(\([[:digit:]]\{1,3\}\.\)\{3\}[[:digit:]]\{1,3\}\)\/search.*$/\1 \2/' | tee ip_reqip | wc -l

# On trie les IPs et on génère les stats de requêtes
echo -n " * Tri, comptage ... [> ip_reqip_stats] Lignes : "
cat ip_reqip | sort | uniq -c | tee ip_reqip_stats | wc -l

# On regarde le reverse DNS de chaque IP destination
echo -n " * Reverse DNS des IPs destination ... [> reverse_dns] Lignes : "
for SEARCHIP in `cat ip_reqip | awk '{print $2}' | sort | uniq`
do
  echo -n "$SEARCHIP " >> reverse_dns
  host $SEARCHIP >> reverse_dns
done
cat reverse_dns | wc -l
cat reverse_dns

# On garde les IPs destination n'ayant pas de reverse DNS
# (peut-être est-ce trop restrictif ?)
echo -n " * Recherche des hôtes compromis ... [> compromised] Lignes : "
for SEARCHIP in `grep " not " reverse_dns | awk '{print $1}'`
do
  echo "  * $SEARCHIP" >> stats
  for NEWIP in `grep $SEARCHIP ip_reqip | awk '{print $1}' | sort | uniq`
  do
    if ! grep $NEWIP compromised > /dev/null
    then
      echo -n "$NEWIP " >> compromised
      host $NEWIP >> compromised
    fi
  done
  grep $SEARCHIP ip_reqip_stats >> stats
done
cat compromised | wc -l
cat compromised

# On affiche les statistiques
echo " * Statistiques ... [> stats]"
cat stats

# Si on a d'anciennes stats, on fait le diff
if [ -f stats.old ]
then
  echo " * Diff ... [> diff]"
  diff -uN stats.old stats | tee diff
fi

# Si on a l'option --mailto, on envoie un mail avec le diff
if [ "$1" == "--mailto" ] && [ -n "$2" ]
then
  if [ -f stats.old ]
  then if [ -s diff ] && [ -s stats ]
    then
      # On envoie le diff (à condition qu'il y ait des machines infectées)
      ( header ; cat diff ) | mail -s "Detection du virus Conficker sur `hostname`" "$2"
    fi
  elif [ -s stats ]
  then
    # S'il n'y avait pas de précédentes stats, on envoie les nouvelles directement
    ( header ; cat stats ) | mail -s "Detection du virus Conficker sur `hostname`" "$2"
  fi
fi
