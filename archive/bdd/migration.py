#! /usr/bin/python

if __name__ == '__main__':
    import sys
    sys.path.append('/usr/scripts/gestion')
    from ldap_crans import crans_ldap
    ldap = crans_ldap()
    
    import connection
    from proprio import Proprio
    from comptes import CompteUnix
    from local import Local
    from comptes import Attribut
    from comptes import GroupeUnix
    
    
    adherents_ldap = ldap.search('nom=*')['adherent'] # Pour les tests, on ne selectionne que les nounous
    #adherents_ldap = []
    for adherent_ldap in adherents_ldap:
        
        data = adherent_ldap._data
        
        args = {}
        
        args['categorie'] = u'adherent'
        args['id_ldap'] = int(adherent_ldap.id())
        args['nom'] = adherent_ldap.nom()
        args['prenom'] = adherent_ldap.prenom()
        args['titre'] = None
        args['naissance'] = None
        

        args['adresse1'] = adherent_ldap.adresse()[0]
        args['adresse2'] = adherent_ldap.adresse()[1]
        args['code_postal'] = adherent_ldap.adresse()[2]
        args['ville'] = adherent_ldap.adresse()[3]
        args['pays'] = None
        
        args['telephone'] = adherent_ldap.tel()
        args['email'] = adherent_ldap.email()
        
        args['etablissement'] = adherent_ldap.etudes(0)
        args['annee_etudes'] =  adherent_ldap.etudes(1)
        args['filiere'] =  adherent_ldap.etudes(2)
        
        args['remarques'] = '\n'.join(adherent_ldap.info())
        args['fictif']  = False
        args['responsableID'] = None
        
        selection = Proprio.select(Proprio.q.id_ldap ==  adherent_ldap.id())
        if selection.count() == 0:
            proprio_pg = Proprio(**args)
        else:
            proprio_pg = selection[0]
            proprio_pg.set(**args)
            
        if 'uid' in adherent_ldap._data:
            args = {}
            args['proprioID'] = proprio_pg.id
            args['username'] = adherent_ldap.mail()
            args['passwd'] = '!'
            args['uid'] = int(adherent_ldap._data['uidNumber'][0])
            args['homedir'] = adherent_ldap._data['homeDirectory'][0]
            args['shell'] = adherent_ldap._data['loginShell'][0]
            args['gecos'] = adherent_ldap._data['gecos'][0]
            args['gid'] = 3
            args['solde'] = adherent_ldap.solde()
            
            selection = CompteUnix.select(CompteUnix.q.proprioID ==  proprio_pg.id)
            if selection.count() == 0:
                compte_pg = CompteUnix(**args)
            else:
                compte_pg = selection[0]
                compte_pg.set(**args)
            
            
            for droit in adherent_ldap.droits():
                selection = Attribut.select(Attribut.q.nom ==  droit)
                if selection.count() == 0:
                    attribut = Attribut(nom=droit)
                else:
                    attribut = selection[0]
                if attribut not in compte_pg.attributs:
                    compte_pg.addAttribut(attribut)
                
        
        print adherent_ldap.nom()
        
    clubs_ldap = ldap.search('cid=*')['club'] # Pour les tests, on ne selectionne que les nounous
    for club_ldap in clubs_ldap:
        
        data = club_ldap._data
        
        args = {}
        
        args['categorie'] = u'club'
        args['id_ldap'] = int(club_ldap.id())
        args['nom'] = club_ldap.nom()
        args['prenom'] = None
        args['titre'] = None
        args['naissance'] = None
        

        args['adresse1'] = None
        args['adresse2'] = None
        args['code_postal'] = None
        args['ville'] = None
        args['pays'] = None
        
        args['telephone'] = None
        args['email'] = None
        
        args['etablissement'] = None
        args['annee_etudes'] =  None
        args['filiere'] =  None
        
        args['remarques'] = '\n'.join(club_ldap.info())
        args['fictif']  = False
        args['responsableID'] = Proprio.select(Proprio.q.id_ldap ==  club_ldap._data['responsable'][0])[0].id
        
        selection = Proprio.select(Proprio.q.id_ldap ==  club_ldap.id())
        if selection.count() == 0:
            proprio_pg = Proprio(**args)
        else:
            proprio_pg = selection[0]
            proprio_pg.set(**args)
            
        if 'uid' in club_ldap._data:
            args = {}
            args['proprioID'] = proprio_pg.id
            args['username'] = club_ldap._data['uid']
            args['passwd'] = '!'
            args['uid'] = int(club_ldap._data['uidNumber'][0])
            args['homedir'] = club_ldap._data['homeDirectory'][0]
            args['shell'] = club_ldap._data['loginShell'][0]
            args['gecos'] = None
            args['gid'] = 3
            args['solde'] = club_ldap.solde()
            
            selection = CompteUnix.select(CompteUnix.q.proprioID == proprio_pg.id)
            if selection.count() == 0:
                compte_pg = CompteUnix(**args)
            else:
                compte_pg = selection[0]
                compte_pg.set(**args)
            
                    
        print club_ldap.nom()
        

        
    nounou = Attribut.select(Attribut.q.nom == u'Nounou')[0]
    selection = GroupeUnix.select(GroupeUnix.q.nom == 'ssh_partout')
    if selection.count() == 0:
        ssh_partout = GroupeUnix(nom='ssh_partout', gid=200, descr='Groupes des utilisateur autorises a se logger partout')
    else:
        ssh_partout = selection[0]
    if ssh_partout not in nounou.groupes_unix:
        nounou.addGroupeUnix(ssh_partout)
        
