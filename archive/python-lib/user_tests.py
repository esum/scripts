#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

""" Fonctions de tests sur l'utilisateur 

Copyright (C) Fr�d�ric Pauget
Licence : GPLv2
"""

import os, commands, pwd

def getuser() :
    """ Retourne l'utilisateur lancant les scripts """
    user = os.getenv('SUDO_USER')
    if not user :
	user = pwd.getpwuid(os.getuid())[0]
    return user
    
def groups(login='') :
    """ Retourne liste des groupes (gid) auquels appartient l'utilisateur 
    fourni, si aucun utilisateur est founit prend l'utilisateur loggu� """
    if login == '' : login = getuser()
    return commands.getoutput('id -G %s' % login).split()

def isadm(login='') :
    """ Retourne True si l'utilisateur est dans le groupe 4 (adm)
    Si login='', prend l'utilisateur loggu� """
    return '4' in groups(login)

def isdeconnecteur(login='') :
    """ Retourne True si l'utilisateur est dans le groupe 610 (disconnect)
    Si login='', prend l'utilisateur loggu� """
    return isadm(login) or '610' in groups(login)

def iscableur(login='') :
    """ Retourne True si l'utilisateur est dans le groupe 604 (respbat)
    Si login='', prend l'utilisateur loggu� """
    return isadm(login) or '604' in groups(login)
