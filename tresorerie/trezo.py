#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

from __future__ import print_function, unicode_literals

from gestion.config.config import modePaiement

ANNEE = 2016

BASE_FILTER = """(&(fid=*)
(recuPaiement>=%d0101000000+0000)(recuPaiement<=%d0101000000+0000))""" % (ANNEE, ANNEE+1)

from lc_ldap.shortcuts import lc_ldap_readonly
conn = lc_ldap_readonly()

def summary(control=False, mode=None):
    f = []
    if control:
        f.append('(controle=TRUE)')
    if mode:
        f.append('(modePaiement=%s)' % mode)
    f.append(BASE_FILTER)

    f = "(&%s)" % (''.join(f))
    factures = conn.search(f, sizelimit=3000)
    
    total = sum(facture.total() for facture in factures)

    return {
        'total': total,
        'nb': len(factures),
        'filtre': f,
    }

for paiement in modePaiement:
    print("= Résumé des paiements par %s =" % paiement)

    d = summary(mode=paiement)
    print("Total: %(total)d € (en %(nb)d factures)" % d)

    d = summary(control=True, mode=paiement)
    print("Dont validées: %(total)d € (%(nb)d factures)" % d)
    
    print("# Filtre ldap: %s\n\n" % d['filtre'].replace('\n',''))


