#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8; mode: python -*-
"""Permet de construire un CSV contenant l'ensemble des contributions
financières des adhérents sur une année civile."""

import argparse
import re
import sys, string

from lc_ldap import shortcuts, objets

from gestion.config import factures as factures_config

COMPTE_MEMBRES = "410 Membres"
COMPTE_COTISATIONS = "756 Cotisation"
# Impressions, accès internet
COMPTE_PRESTATIONS = "7061 Contribution accès internet"
COMPTE_SOLDE = "7062 Crédit solde"
COMPTE_OBJET = "707 Vente de marchandise"

FIELD_SEP = ','

CHAMPS = [
    'date',
    'description',
    'amount',
    'account1',
    'account2'
]


VENTE_ITEMS = [
    unicode(key)
    for key in factures_config.ITEMS.keys()
]

def collect_objets(annee):
    """Récupère les données pour l'année fournie"""

    out = []

    conn = shortcuts.lc_ldap_readonly()

    __filtre = u"(&(fid=*)(recuPaiement>=%(annee)s0101000000+0100)(recuPaiement<=%(annee)s1231235959+0100)(!(controle=FALSE)))" % {
        'annee': annee,
    }

    factures = conn.search(
        __filtre,
        sizelimit=0,
    )

    for facture in factures:
        if facture['modePaiement'][0] == u'solde':
            continue
        date = facture['recuPaiement'][0].value.strftime("%d/%m/%Y")
        _proprio = facture.proprio()
        description = "%s %s" % (
            _proprio['prenom'][0] if isinstance(_proprio, objets.adherent) else "Club",
            _proprio['nom'][0],
        )
        for article in facture['article']:
            if not article['code'] in VENTE_ITEMS or float(article['pu']) == 0.0:
                continue
            memo = u"Vente de %s" % (article["designation"],)
            tn = str(article['pu'])

            out.append({
                'date': date,
                'description': "%s à %s" % (memo.encode('utf-8'), description,),
                'amount': tn,
                'account1': COMPTE_MEMBRES,
                'account2': COMPTE_OBJET,
            })

    with open("/usr/scripts/var/compta_vente_obj.csv", 'w') as csv_file:
        csv_file.write(FIELD_SEP.join(CHAMPS) + "\n")
        for entry in out:
            csv_file.write(
                "%s\n" % (
                    FIELD_SEP.join(
                        [
                            entry[champ]
                            for champ in CHAMPS
                        ],
                    ),
                ),
            )


if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description="Production d'un CSV à partir des factures.", add_help=False)
    PARSER.add_argument('-o', '--objets', action='store_true', help="Rapport des ventes d'objets")
    PARSER.add_argument('-h', '--help', action='store_true', help='Affiche cette aide et quitte.')
    PARSER.add_argument('annee', type=str, nargs=1, help="L'année pour laquelle le CSV est à produire.")

    ARGS = PARSER.parse_args()

    if ARGS.help:
        PARSER.print_help()
        sys.exit(0)

    if ARGS.objets:
        collect_objets(ARGS.annee[0])
