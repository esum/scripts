#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8; mode: python -*-
"""Parcourt l'intégralité de l'historique des clubs et/ou adhérents dans ldap
pour avoir le bilan de ce que l'imprimante a rapporté pendant l'année"""

import argparse
import sys

from lc_ldap import shortcuts

def sum_list(l, annee):
    somme = 0
    for obj in l:
        for hist in obj['historique']:
            if hist.get_datetime().year == annee:
                hist = str(hist)
                l_hist = hist.split()
                if len(l_hist) >= 8:
                    # ce sont les "impression" effectuées
                    if "[impression" in l_hist[7]:
                        val = float(l_hist[5])
                        somme = somme + val
                    # ce sont les "Impression ratee"
                    elif "[Impression" in l_hist[7]:
                        val = float(l_hist[5])
                        somme = somme - val
    return(somme)


if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description="Effectue le bilan comptable de l'imprimante", add_help=False)
    PARSER.add_argument('-h', '--help', action='store_true',
            help="Affiche cette aide et quitte.")
    PARSER.add_argument('-a', '--adherents', action="store_true",
            help="Effectue la comptabilité des adhérents uniquements (défaut : clubs et adhérents).")
    PARSER.add_argument('-c', '--clubs', action="store_true",
            help="Effectue la comptabilité des clubs uniquements (défaut : clubs et adhérents).")
    PARSER.add_argument('-t', '--test', action="store_true",
            help="Inclure le compte de test dans la recherche")
    PARSER.add_argument('annee', type=str, nargs=1,
            help="L'année pour laquelle on cherche l'information.")

    ARGS = PARSER.parse_args()

    if ARGS.help:
        PARSER.print_help()
        sys.exit(0)

    ANNEE = int(ARGS.annee[0])

    conn = shortcuts.lc_ldap_readonly()

    # On fait dans tous les cas la recherche des objets : ça coûte pas
    # grand chose et ça rend le code plus propre
    clubs = conn.search(u"cid=*", sizelimit=0)
    if ARGS.test:
        adherents = conn.search(u"aid=*", sizelimit=0)
    else:
        # On fait le pari que le seul compte de test est et restera Toto Passoir
        adherents = conn.search(u"(&(aid=*)(!(aid=4281)))", sizelimit=0)

    if ARGS.clubs:
        print(sum_list(clubs, ANNEE))

    elif ARGS.adherents:
        print(sum_list(adherents, ANNEE))

    else:
        print(sum_list(adherents, ANNEE) + sum_list(clubs, ANNEE))
