# Copyright (C) 2001-2003 by the Free Software Foundation, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software 
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

"""MailList mixin class managing the spamassassin options.
"""

import re

from Mailman import mm_cfg
from Mailman import Utils
from Mailman.i18n import _
from Mailman.Gui.GUIBase import GUIBase

# Valeurs par defaut
default_hold = 2.1
default_discard = 8
default_member = 2

try:
    True, False
except NameError:
    True = 1
    False = 0

class SpamAssassin(GUIBase):
    def GetConfigCategory(self):
        return 'spamassassin', _('SpamAssassin options...')

    def GetConfigInfo(self, mlist, category, subcat=None):
        if category <> 'spamassassin':
            return None

        # Si la ML n'a pas de valeurs configurees, on initialise avec les valeurs par defaut
        if (getattr(mlist,'SPAMASSASSIN_HOLD_SCORE',None)==None
                or getattr(mlist,'SPAMASSASSIN_DISCARD_SCORE',None)==None
                or getattr(mlist,'SPAMASSASSIN_MEMBER_BONUS',None)==None):
            setattr(mlist, 'SPAMASSASSIN_HOLD_SCORE', default_hold)
            setattr(mlist, 'SPAMASSASSIN_DISCARD_SCORE', default_discard)
            setattr(mlist, 'SPAMASSASSIN_MEMBER_BONUS', default_member)
            
        return [('SPAMASSASSIN_HOLD_SCORE',mm_cfg.Number,7,0,_('''minimum SpamAssassin score before message moderation'''),_('''If the message have a SpamAssassin score higher than this value then it is deferred for moderation.''')),
                ('SPAMASSASSIN_DISCARD_SCORE',mm_cfg.Number,7,0,_('''minimum SpamAssassin score before discarding the message'''),_('''If the message have a SpamAssassin score higher than this value then it is discard.''')),
                ('SPAMASSASSIN_MEMBER_BONUS',mm_cfg.Number,7,0,_('''Bonus score for message coming from a member of the list'''),_('''If the message comes from a member of the list then the SpamAssassin score is lessen by the value.'''))]
