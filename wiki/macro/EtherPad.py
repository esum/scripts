# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - EtherPad Macro
    Benjamin « esum » « coq » Graillot
    pour le Cr@ns

    Syntaxe :
    <<EtherPad(name)>>
    <<EtherPad(name, show_controls=true, ...)>>

    Arguments:
    * name: nom du pad.

    Arguments optionnels:
    * show_controls: afficher les contrôles du pad.
    * show_chat: afficher le clavardage.
    * show_line_numbers: afficher les numéros de ligne.
    * use_monospace_font: utiliser la police à chasse fixe.
    * height: hauteur du pad en pixer.
    * frameborder: afficher le cadre autour du pad.
"""

def execute(macro, text):
    text = text.split(',')
    name = text[0].strip()
    args = [name]
    kwargs = {}
    for arg in text[1:]:
        kw = None
        if '=' in arg:
            arg = arg.split('=')
            kw = arg[0].strip()
            arg = arg[1]
        arg = arg.strip()
        if arg.lower() in {'true', 'false'}:
            arg = bool(arg)
        elif arg.isnumeric():
            arg = int(arg)
        if kw is None:
            args.append(arg)
        else:
            kwargs[kw] = arg
    return macro.formatter.rawHTML(html(*args, **kwargs))

def html(name, show_controls=None, show_chat=None, show_line_numbers=None, use_monospace_font=True, height=400, frameborder=True):
    readonly = name.startswith('r.')
    if not isinstance(show_controls, bool):
        show_controls = not readonly
    if not isinstance(show_chat, bool):
        show_chat = not readonly 
    if not isinstance(show_line_numbers, bool):
        show_line_numbers = not readonly
    html = u'''<iframe name="{}" src="https://pad.crans.org/p/{}?showControls={}&showChat={}&showLineNumbers={}&useMonospaceFont={}" width="100%" height="{}" frameborder="{}"></iframe>'''.format(
        'embed_readonly' if readonly else 'embed_readwrite',
        name,
        repr(show_controls).lower(),
        repr(show_chat).lower(),
        repr(show_line_numbers).lower(),
        repr(use_monospace_font).lower(),
        height,
        int(frameborder))
    return html
