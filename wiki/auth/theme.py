# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - CAS authentication

    Jasig CAS (see http://www.jasig.org/cas) authentication module.

    @copyright: 2012 MoinMoin:RichardLiao
    @license: GNU GPL, see COPYING for details.
"""

import sys
import os
import time, re
import urlparse
import urllib, urllib2
from netaddr import IPNetwork, IPAddress

from werkzeug import get_host

from MoinMoin import log
logging = log.getLogger(__name__)

from MoinMoin.auth import BaseAuth, ContinueLogin
from MoinMoin import user, wikiutil
from MoinMoin.theme import load_theme_fallback


class ThemeVhost(BaseAuth):
    name = 'ThemeVhost'
    login_inputs = []
    logout_possible = False

    def __init__(self, theme_vhost={}):
        BaseAuth.__init__(self)
        self.theme_vhost=theme_vhost;

    def request(self, request, user_obj, **kw):
        host = get_host(request.environ)
        if not user_obj:
            theme_name = self.theme_vhost.get(host, request.user.theme_name)
        else:
            theme_name = self.theme_vhost.get(host, user_obj.theme_name)

        load_theme_fallback(request, theme_name)
        return user_obj, True

    def login(self, request, user_obj, **kw):
        return ContinueLogin(user_obj)

    
    def logout(self, request, user_obj, **kw):
        load_theme_fallback(request, request.cfg.theme_default)
        return user_obj, True

    def login_hint(self, request):
        _ = request.getText
        msg = _(u'<p>Plus d\'informations sur comment cr&eacute;er un compte sont disponibles sur la page <a href="/Cr&eacute;erUnCompteWiki">Cr&eacute;erUnCompteWiki</a></p>')
        return msg
