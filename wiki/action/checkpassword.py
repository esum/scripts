# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - Cr@ns : checkpassword current-password

    @author: daniel.stan@crans.org

    @copyright: 2000-2004 Juergen Hermann <jh@web.de>,
                2006 MoinMoin:ThomasWaldmann,

    @license: GNU GPL, see COPYING for details.

    To link to /usr/lib/pymodules/python2.6/MoinMoin/action
"""
from MoinMoin import user

def execute(pagename, request):
    """ Check password """
    _ = request.getText
    values = request.values
    username = values.get('username', '')
    password = values.get('password', '')

    u = user.User(request, name=username, password=password, auth_method='moin')
    if u.valid:
       request.write('OK') 
    else:
       request.write('Wrong')
