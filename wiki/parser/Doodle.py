# -*- coding: iso-8859-1 -*-
"""
                             _vi
                             <Ii           _aa.
                             :I>          aZ2^
                              v`        .we^
        . .         ..        +        _2~
  ._auqXZZX   ._auqXZZZ`    ...._...   ~          ._|ii~
.aXZZY""^~~  vX#Z?""~~~._=ii|+++++++ii=,       _=|+~-
JXXX`       )XXX'    _|i+~   .__..._. +l=    -~-
SXXo        )XZX:   |i>  ._%i>~~+|ii| .i|   ._s_ass,,.    ._a%ssssssss
-SXZ6,,.    )XZX:  =l>  _li+~`   iii| .ii _uZZXX??YZ#Za, uXUX*?!!!!!!!
  "!XZ#ZZZZZXZXZ`  <i>  =i:     .|ii| .l|.dZXr      4XXo.XXXs,.
      -~^^^^^^^`   -||,  +i|=.   |ii| :i>:ZXZ(      ]XZX.-"SXZUZUXoa,,
                     +l|,  ~~|++|++i|||+~:ZXZ(      ]ZZX     ---~"?Z#m
            .__;=-     ~+l|=____.___,    :ZXZ(      ]ZXX_________auXX2
       ._||>+~-        .   -~+~++~~~-    :ZXZ(      ]ZXZZ#######UX*!"
       -+--          .>`      _
                   .<}`       3;
                 .<l>        .Zc
                .ii^         )Xo
                             ]XX

    MoinMoin - Doodle poll parser

    PURPOSE:
        un joli sondage doodle sur le wiki

    AUTHOR:
        Antoine Durand-Gasselin <adg@crans.org>

    CALLING SEQUENCE:
       {{{
       #!Doodle pollname; choix1; choix2; choix3
       participant1; choix1=1; choix2=0; choix3=1
       participant2; choix1=0; choix2=1; choix3=1
       }}}
"""

import os, string, re, StringIO, base64
from MoinMoin import config, wikiutil
from MoinMoin.parser.text_moin_wiki import Parser as WikiParser

Dependencies = []

class Parser:
    parsername = "Doodle"
    Dependencies = Dependencies

    def __init__ (self, raw, request, **kw):
        self.form = request.form
        self.request = request
        self.raw = raw
        self.parseArgs(kw["format_args"])

    def parseArgs(self, argsString):
        argList = argsString.split(';')
        self.poll_name=argList[0]
        self.poll_choices=filter ((lambda x: x != ""), [ i.strip() for i in argList[1:]])

    def format(self, f):
        entries = self.raw.splitlines()
        stylefmt = 'background-color: %s; border: 1px solid %s;'
        code = '\n\n<form action="?action=fillpoll&amp;pollname=%s" method="post">' % self.poll_name
        code += f.table(1)
        code += f.table_row(1)
        code += f.table_cell(1)
        code += f.table_cell(0)
        for choice in self.poll_choices:
            code += f.table_cell(1)
            code += f.text(choice)
            code += f.table_cell(0)
        code += f.table_row(0)
        for entry in entries:
            code += f.table_row(1)
            code += f.table_cell(1)
            code += f.text(entry.split(";",1)[0].strip())
            code += f.table_cell(0)
            for choice in self.poll_choices:
                resp = re.search(re.escape(choice)+'=?', entry)
                if resp:
                    disp = entry[resp.end()]
                    if '1' == disp: colors= ('#a8ffa6', '#64d06a')
                    elif '0' == disp: colors= ('#ffaca8', '#e88782')
                    else: color = ('grey', 'grey')
                    code += f.table_cell(1,{'style': stylefmt% colors})
                    code += f.text(disp)
                else:
                    code += f.table_cell(1,{'style': stylefmt % ('grey', 'grey')})
                    code += f.text('nsp')
                code += f.table_cell(0)
            code += f.table_row(0)

        code += f.table_row(1)
        code += f.table_cell(1)
        code += ('<input type="text" name="user" />')
        code += f.table_cell(0)

        for choice in self.poll_choices:
            if isinstance(choice, unicode):
                # l'utf-8, c'est bien! On encode l'unicode en utf-8 avant
                # de le base64er
                name = base64.encodestring(choice.strip().encode('utf-8')).strip('\r\t \n=')
            else:
                # Bon, si on n'a pas un unicode, on encode sauvagement, mais
                # �a peut chier
                name = base64.encodestring(choice.strip()).strip('\r\t \n=')

            code += f.table_cell(1)
            code += '<input type="checkbox" name="%s" value="1">' % name
            code += f.table_cell(0)

        code += f.table(0)
        code += '<input type="submit" name="boutok" value="envoyer"/>'
        code += '</form>'

        self.request.write(code)

